/**
 * Constants to define directions elevators are traveling and buttons pressed
*/
const IDLE = 0;
const UP = 1;
const DOWN = -1;

/**
 * Constants defining Elevator statuses
 */
const WAITING = 'WAITING';

/**
 * Controller is the main class responsible for initializing the elevator system.
 * Creates floors, elevators, and handles requests from floors and elevators.
 * 
 * subclasses
 * ...none yet
 */
class Controller {
  constructor(numFloors = 2, numElevators = 1) {
    this.floors = [];
    this.elevators = [];
    this.requests = [];

    let i;
    for (i = 0; i < numFloors; i++) {
      this.floors.push(new Floor(i));
    }

    for (i = 0; i < numElevators; i++) {
      this.elevators.push(new Elevator(i));
    }

    this.handleRequestSignal = this.handleRequestSignal.bind(this);
  }

  handleRequestSignal(floor, direction) {
    console.log(`Request from floor ${floor} to go ${direction > 0 ? 'up' : 'down'}.`);

    if (floor === 0 && direction === DOWN || (floor === this.floors.length) && direction === UP) {
      console.error('Cannot go that direction!'); // Would normally throw an error here but just using console error for purposes of this exercise...
      return;
    }


    // 1. Go through elevators and check which ones are available (this will come from elevator's status or maybe a new attribute for elegibility).
    // 2. Go through list of eligible elevators and check which on is closest.
    // 3. Assign the closest elevator the job of meeting the current request.
    // 4. Handle case for no elevators being available?

  }
}


/**
 * Elevator describes a single elevator in the system
 * 
 * @param {number} elevatorNumber The Elevator's number identifier
 *
 */
class Elevator {
  constructor(elevatorNumber) {
    this.number = elevatorNumber;
    this.direction = IDLE; // Which way the elevator is currently traveling (see constants at top of file for definitions)
    this.restingFloor = 0; // Where the elevator is currently resting/stopped (-1 is moving)
    this.status = WAITING; // What is the elevator currently doing? (see constants at top of file for definitions)

    this.handleRequest = this.handleRequest.bind(this);
  }

  handleRequest () { // This will be used to move the elevator should the controller ask it to go somewhere

  }

  eligibleForRequest (requestDirection, requestFloor) { // This would be called by the controller to check if an elevator meets the controller's requirements
    if (this.restingFloor === requestFloor && status === WAITING) {
      return 0;
    }

    // Need to fill out the rest of the eligibility logic...
  }
}

/**
 * CallButton describes an up/down button to call an elevator
 * 
 * Could probably be extended to act as buttons for other parts of the system, such as
 * in-elevator buttons, but is specific to floors for now.
 * 
 * @param {number} floor The floor id where the button is located
 * @param {number} direction The direction the button will call (1 === up & -1 === down)
 *
 */
class CallButton extends Controller {
  constructor(floor, direction) {
    super(floor, direction);
    this.buttonFloor = floor;
    this.buttonType = direction;

    this.onPress = this.onPress.bind(this);
  }

  onPress() {
    this.handleRequestSignal(this.buttonFloor, this.buttonType);
  }
}


/**
 * Floor describes a single floor in the system
 * 
 * @param {number} floorNumber The Floor's number identifier
 *
 */
class Floor {
  constructor(floorNumber) {
    this.floor = floorNumber;
    this.buttonUp = new CallButton(floorNumber, UP);
    this.buttonDown = new CallButton(floorNumber, DOWN);
  }
}

// Initialize a new elevator system...
const buildingElevators = new Controller(10, 2);

buildingElevators.floors[0].buttonUp.onPress();
buildingElevators.floors[9].buttonUp.onPress();
